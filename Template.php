<?php
App::uses('AuthComponent', 'Controller/Component');
 
class Template extends AppModel {
     
    public $avatarUploadDir = 'img/avatars';
     
    public $validate = array(
	       'name' => array(
            'nonEmpty' => array(
                'rule' => array('notEmpty'),
				 'required' => true,
                'message' => 'name is required',
                'allowEmpty' => false
            ),
			'pattern'=>array(
                 'rule'      => '/^([a-zA-Z0-9]+\s?)*$/',
                    'message'   => 'does not allow special characters',
            ),
      
        ),
		'content' => array(
            'nonEmpty' => array(
                'rule' => array('notEmpty'),
				 'required' => true,
                'message' => 'content is required',
                'allowEmpty' => false
            ),
			
      
        ),
		'schedule' => array(
            'nonEmpty' => array(
                'rule' => array('notEmpty'),
				 'required' => true,
                'message' => 'schedule is required',
                'allowEmpty' => false
            ),
      
        ),
		'schedule_specific' => array(
            'nonEmpty' => array(
                'rule' => array('notEmpty'),
				 'required' => true,
                'message' => 'schedule specific is required',
                'allowEmpty' => false
            ),
      
        ),
		'schedule_time' => array(
            'nonEmpty' => array(
                'rule' => array('notEmpty'),
				 'required' => true,
                'message' => 'schedule time is required',
                'allowEmpty' => false
            ),
      
        ),
		'type' => array(
            'nonEmpty' => array(
                'rule' => array('notEmpty'),
				 'required' => true,
                'message' => 'type is required',
                'allowEmpty' => false
            ),
      
        ),
		'status' => array(
            'nonEmpty' => array(
                'rule' => array('notEmpty'),
				 'required' => true,
                'message' => 'status is required',
                'allowEmpty' => false
            ),
      
        )

 
         
    );
     
        /**
     * Before isUniqueUsername
     * @param array $options
     * @return boolean
     */
    function isUniqueUsername($check) {
 
        $username = $this->find(
            'first',
            array(
                'fields' => array(
                    'User.id',
                    'User.username'
                ),
                'conditions' => array(
                    'User.username' => $check['username']
                )
            )
        );
 
        if(!empty($username)){
            if($this->data[$this->alias]['id'] == $username['User']['id']){
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }
 
    /**
     * Before isUniqueEmail
     * @param array $options
     * @return boolean
     */
    function isUniqueEmail($check) {
 
        $email = $this->find(
            'first',
            array(
                'fields' => array(
                    'User.id'
                ),
                'conditions' => array(
                    'User.email' => $check['email']
                )
            )
        );
 
        if(!empty($email)){
            if($this->data[$this->alias]['id'] == $email['User']['id']){
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }
     
    public function alphaNumericDashUnderscore($check) {
        // $data array is passed using the form field name as the key
        // have to extract the value to make the function generic
        $value = array_values($check);
        $value = $value[0];
 
        return preg_match('/^[a-zA-Z0-9_ \-]*$/', $value);
    }
     
    public function equaltofield($check,$otherfield)
    {
        //get name of field
        $fname = '';
        foreach ($check as $key => $value){
            $fname = $key;
            break;
        }
        return $this->data[$this->name][$otherfield] === $this->data[$this->name][$fname];
    }
 
    /**
     * Before Save
     * @param array $options
     * @return boolean
     */
     public function beforeSave($options = array()) {
        // hash our password
        if (isset($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
        }
         
        // if we get a new password, hash it
        if (isset($this->data[$this->alias]['password_update']) && !empty($this->data[$this->alias]['password_update'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password_update']);
        }
     
        // fallback to our parent
        return parent::beforeSave($options);
    }
 
}