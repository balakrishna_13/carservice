<style>

.col-md-5

{

padding-left:0px !important;

padding-right:0px !important;

background-color:#fff;



}

.box{



box-shadow:none;

border-radius:none;

}

.col-md-12 *

{

font-size:13px;

}

.form-control {



font-size: 13px;

height: 30px;

line-height: 1;

padding: 1px 9px;

width:50% !important;



}

.col-md-6

{

background-color:white !important;	

}

.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {

line-height: 2 !important;

padding-bottom: 0 !important;

padding: 0px !important;

vertical-align: middle !important;

text-transform: capitalize;

}

.table > thead > tr > th {

border-bottom: 0px solid #ddd !important;

vertical-align: middle !important;

text-transform: capitalize;

padding-bottom: 0 !important;

padding-top: 2px !important;

}

td{

color:#338585;

margin-left:5px;

padding:5px;

}

.table-striped > tbody > tr:nth-child(2n+1) > td{

background-color:white;

}



.btn

{

line-height:1;

font-size: 13px;

padding: 6px 4px;

}



.table > thead > tr > th {

padding-bottom: 5px !important;	

}

th{			

padding:5px;

}

.back-white{

background-color:#fff;

}

.header2{

font-size:18px;

}

.tdd{

width:190px;

}

.tds{

width:30px;

}

.tdh{

width:100px;
align:center !important;

}

.page-header

{

font-size:18px !important;

padding-left:2px;

}

.message {

    margin-left: 26% !important;

    width: 40% !important;

	background-color:green !important;

}

.success, .message, .cake-error, p.error, .error-message

{

	background-color:green !important;	

}

table tr:nth-child(2n)

{

	background:#fff !important;

}

.wrapper.row-offcanvas.row-offcanvas-left > div

{

	padding-bottom:90px !important;

}
 
ul.tabs {
    margin: 0;
    padding: 0;
    float: left;
    list-style: none;
    height: 32px;
    width: 100%;
}
ul.tabs li {
     
    float: left;
    margin: 0;
    padding: 0;
    height: 31px;
    line-height: 31px;
    border: 1px solid #999;
    border-left: none;
    margin-bottom: -1px;
    background: #F0F0F0;
    overflow: hidden;
    position: relative;
}
ul.tabs li a {
    text-decoration: none;
    color: #000;
    display: block;
    font-size: 1.2em;
    padding: 0 20px;
    border: 1px solid #fff;
    outline: none;
}
.tabs li
{
color:white;
width:100px;
}
.btn
{
font-size:13px !important;
margin-left:1px !important;
height:30px !important;
} 
.wid{
margin-top:30px;	
}	
</style>
		
	
<aside class="right-side back-white">

		<section>

			<div class="container-fluid back-white">

				<div class="col-md-12 back-white">

					<div  class="col-md-12">

					<?php

					//echo "<pre>";

					//print_r($vehicles_details);

					?>

						<h2 class="page-header">

					Servicing Details					

					
						</h2>  					

					</div>
					
				 
				 	<ul class="tabs">
					<li id="succss"  class='btn btn btn-primary'> Completed </li>
					<li id="failed" class='btn btn-primary'> Pending </li>
					 
					</ul>
				   
			 
				    <br/>
				    <br/> 
				 
					<div class="col-md-12 wid" id="succ">
					<h2 class="page-header"> Servicing Completed	</h2> 
						<table class="table table-striped"  align="center">

							<thead>

								<tr>
								<th> Sl.No </th>	
								<th> Vehicle No </th>	

								<th> No.of Services </th>	

								<th> Date of Servicing </th>

								<th> Remaining Services </th>

								<th> Service Status </th>	

								</tr>      

							</thead>

							<tbody>

								<?php

									$i=1;

									

								

									

									foreach($servicing_completed as $service)
		
									{ 	
										//echo "</pre>";
										//print_r($servicing_completed); exit;
								?>

								<tr>

									<td class='tdh'> <?php echo $i; ?> </td>		

									<td class='tdh'> <?php echo $service['Service']['vehicle_id']; ?> </td>
									<td class='tdh'> <?php echo $service['Service']['NoOfService']; ?> </td>
									<td class='tdh'> <?php echo $service['Service']['servicing_date']; ?> </td>
									<td class='tdh'> <?php echo $service['Service']['remaining']; ?> </td>
									<td class='tdh'> <?php echo $service['Service']['status']; ?> </td>
								 
							 

										 
								</tr>

							<?php 

								$i++;

							} 

							?>							 

							</tbody>							

						</table>
						
						

					</div>	
					
					<div class="col-md-12 wid" id="fail">
					<h2 class="page-header"> Servicing Pending	</h2> 
						<table class="table table-striped"  align="center">

							<thead>

								<tr>
								<th> Sl.No </th>
								
								<th> Vehicle No </th>	

								<th> No.of Services </th>	

								<th> Next Servicing </th>

								<th> Remaining Services </th>

								<th> Service Status </th>	
								
							 
								</tr>      

							</thead>

							<tbody>

								<?php

									$i=1;

									

								 

									

									foreach($servicing_pending as $service)

									{ 

								?>
										
								<tr>

									<td class='tdh'> <?php echo $i; ?> </td>		

									<td class='tdh'> <?php echo $service['Service']['vehicle_id']; ?> </td>
									<td class='tdh'> <?php echo $service['Service']['NoOfService']; ?> </td>
									<td class='tdh'> <?php echo $service['Service']['next_service_date']; ?> </td>
									<td class='tdh'> <?php echo $service['Service']['remaining']; ?> </td>
									<td class='tdh'> <?php echo $service['Service']['status']; ?> </td>
								 
							 

										 
								</tr>

							<?php 

								$i++;

							} 

							?>							 

							</tbody>							

						</table>
						
						

					</div>	

					
					

				</div>

			</div>

		</section>

	</aside>
 

	 
<script language="javascript">
jQuery(document).ready(function(){
   $("#succ").show();	
   $("#fail").hide();
 $("#succss").css("background-color","#142C50");   
$("#succss").click(function(){
    $("#fail").hide();
	$("#succ").show();
$("#succss").css("background-color","#142C50");
$("#failed").css("background-color","#3c8dbc"); 
});

$("#failed").click(function(){
    $("#succ").hide();
	$("#fail").show();
    $("#failed").css("background-color","#142C50");
	$("#succss").css("background-color","#3c8dbc");	
});
    });




</script>	
 
 